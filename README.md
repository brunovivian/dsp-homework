# DSP HomeWork01

## Overview

Nosso projeto consiste  em aplicar filtros, para que melhore os dados e elimine rúidos indesejados, assim sendo possível identificar problemas com maior precisão. Neste projeto, demonstraremos o passo a passo utilizado ao aplicar filtros em dados de ECG para verificar se ocorre arritimia ( uma mudança no ritmo regular e/ou na frequência das batidas do coração ), estes dados são disponibilizados do banco de dados do MIT-BH, e podem ser encontrados em:  https://physionet.org/content/mitdb/1.0.0/ . Nas imagens são demonstradas a representação de como fazer a aquisição de dados caso necessário, e, a ilustração das ondas de um coração, respectivamente. (Exemplo de pseudo código para a aquisição e armazenamento de dados disponibilizado no em "(include/EXEMPLOaquisicao.h).

**Como aquisitar dados de ECG:**

![](figs/AD8232-ESP32.jpg)

Fonte: Autoria Própria.

**Ilustração das ondas do coração:**

![](figs/ELETROCARDIOGRAMA.png)

Fonte: https://geekymedics.com/understanding-an-ecg/

Iniciamos nosso trabalho aplicando DFT nos dados de ECG, e posteriormente aplicamos um filtro passa baixa para inibir ruídos em baixa frequência, e, logo em seguida aplicamos outro filtro passa faixa para remover o ruído de locomoção da pessoa durante o momento da aquisição dos dados.

**DFT:**

![](figs/DFT.png)

Fonte: Autoria Própria.

**Filtro Passa Baixa:**

![](figs/PASSABAIXAfiltro.png)

Fonte: Autoria Própria.

**Sinal Filtrado:**

![](figs/PASSABAIXA.png)

Fonte: Autoria Própria.

**Filtro Passa Faixa:**

![](figs/PASSAFAIXAfiltro.png)

Fonte: Autoria Própria.

**Sinal Após Filtragem Passa Baixa + Passa Faixa:**

![](figs/PASSAFAIXA.png)

Fonte: Autoria Própria.


Os resultados acima tiveram certa distorção, o que nos levou a testar outros filtros. Portanto, novamente aplicamos a FFT nos dados do ECG, seguido da aplicação de um filtro notch (stop band) para inibir ruídos na faixa de 60Hz, e posteriormente aplicamos um outro filtro passa faixa, para novamente remover ruídos de locomoção da pessoa durante o momento da aquisição dos dados . Dessa maneira, foi possível chegar no resultado com menor distorção.

**DFT:**

![](figs/DFT.png)

Fonte: Autoria Própria.

**Filtro Stop Band:**

![](figs/STOPBANDfiltro.png)

Fonte: Autoria Própria.

**Sinal Filtrado:**

![](figs/STOPBAND.png)

Fonte: Autoria Própria.

**Filtro Passa Faixa:**

![](figs/PASSAFAIXA2filtro.png)

Fonte: Autoria Própria.

**Sinal Após Filtragem Stop Band + Passa Faixa:**

![](figs/PASSAFAIXA2.png)

Fonte: Autoria Própria.


Para melhor visualização das ondas do batimento cardíaco demonstraremos os dados em um período de tempo menor(2.8 segundos):

**Melhor visualização:**

![](figs/DADOSFINAIS.png)

Fonte: Autoria Própria.



## Authors:
    - Bruno Vivian
    - Hector Dallanora
    - Gustavo Monrroy
    