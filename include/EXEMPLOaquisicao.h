#include <Arduino.h>

// Defining the pin that connect the sensor AD8232 to the ESP32
const int ad8232Pin = 34;  // Analog pin connected to the sensor 

// Array to samples storage 
const int numSamples = 3600;
int samples[numSamples];

// Next sample Index
int sampleIndex = 0;

void setup() {
  // Initializing serial communication for debugging
  Serial.begin(115200);

  // configuring pin  34 as input 
  pinMode(ad8232Pin, INPUT);

  // Initializing data collect...
  Serial.println("Iniciando coleta de dados do AD8232...");
}

void loop() {
  if (sampleIndex < numSamples) {
    // Reading AD8232 value
    int value = analogRead(ad8232Pin);

    // Storing value on the array
    samples[sampleIndex] = value;

    // Print on the serial port
    Serial.print("Amostra ");
    Serial.print(sampleIndex);
    Serial.print(": ");
    Serial.println(value);

    // Increases the sample index
    sampleIndex++;

    // Wait 1 sec (1000 ms)
    delay(1000);
  } else {
    // If all samples have been collected, stop collecting data
    Serial.println("Coleta de dados completa.");
    while (1);
  }
}